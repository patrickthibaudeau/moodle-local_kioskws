<?php
/**
 * *************************************************************************
 * *                              kioskws                               **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  kioskws                                                 **
 * @name        kioskws                                                 **
 * @copyright   oohoo.biz                                                 **
 * @link        http://oohoo.biz                                          **
 * @author                                                                **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * *************************************************************************/
defined('MOODLE_INTERNAL') || die();

/**
 * Creates the Moodle page header
 * @global stdobject $CFG
 * @global moodle_database $DB
 * @param string $url Current page url
 * @param string $pagetitle  Page title
 * @param string $pageheading Page heading (Note hard coded to site fullname)
 * @param array $context The page context (SYSTEM, COURSE, MODULE etc)
 * @return HTML Contains page information and loads all Javascript and CSS
 */
function local_kioskws_page($url, $pagetitle, $pageheading, $context, $pagelayout = 'base') {
    global $CFG, $PAGE, $SITE;

    $PAGE->set_url($url);
    $PAGE->set_title($pagetitle);
    $PAGE->set_heading($pageheading);
    $PAGE->set_pagelayout($pagelayout);
    $PAGE->set_context($context);
    $PAGE->requires->jquery();
    $PAGE->requires->jquery_plugin('ui');
    $PAGE->requires->jquery_plugin('ui-css');
    $PAGE->requires->jquery_plugin('datatable', 'local_kioskws');
    $PAGE->requires->jquery_plugin('select2', 'local_kioskws');
    $PAGE->requires->jquery_plugin('kioskws', 'local_kioskws');
}

