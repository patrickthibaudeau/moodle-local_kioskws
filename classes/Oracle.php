<?php

/**
 * *************************************************************************
 * *                              suppeval                                **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  suppeval                                                  **
 * @name        suppeval                                                  **
 * @copyright   Glendon York University                                   **
 * @link        http://www.glendon.yorku.ca                               **
 * @author      Patrick Thibaudeau                                        **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * ************************************************************************ */

namespace local_kioskws;

/**
 * Description of oracle
 *
 * @author patrick
 */
class oracle {

    private $dbusername;
    private $dbpassword;
    private $dbhost;
    private $conn;

    /**
     * 
     * @global \stdClass $CFG
     */
    public function __construct($dbHost = '', $dbUser = '', $dbPwd = '') {
        global $CFG;

        if (empty($dbHost)) {
            $this->dbusername = $CFG->kioskwsDbUsername;
            $this->dbpassword = $CFG->kioskwsDbPwd;
        } else {
            $this->dbusername = $dbUser;
            $this->dbpassword = $dbPwd;
            $this->dbhost = $dbHost;
        }
    }

    function getDbusername() {
        return $this->dbusername;
    }

    function getDbpassword() {
        return $this->dbpassword;
    }

    function getDbhost() {
        return $this->dbhost;
    }

    private function connect() {
        global $CFG;
        $db = $CFG->kioskwsdbconnection;
        $this->conn = oci_connect($this->dbusername, $this->dbpassword, $db);
        if (!$this->conn) {
            $e = oci_error();
            trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
        }

        return is_resource($this->conn) ? true : false;
    }

    /**
     * 
     * @param string $table Table name. Include schema if required
     * @param array $condition An array of AND column => value
     * @param string $sort Normal SQL sort definition
     */
    public function get_records($table, $condition = array(), $sort = '') {
        $conn = $this->connect();

        $where = '';
        foreach ($condition as $key => $value) {
            $where .= $key . " = '" . $value . "' AND ";
        }

        $where = rtrim($where, ' AND ');

        if ($where != '') {
            $where = 'WHERE ' . $where;
        }

        $sql = "SELECT * FROM $table $where $sort";

        $stid = oci_parse($this->conn, $sql);

        oci_execute($stid);
        $count = oci_fetch_all($stid, $all, null, null, OCI_FETCHSTATEMENT_BY_ROW + OCI_ASSOC);
        oci_close($this->conn);
        return $all;
    }

    public function get_records_sql($sql) {
        $conn = $this->connect();
        $stid = oci_parse($this->conn, $sql);
        oci_execute($stid);
        $count = oci_fetch_all($stid, $all, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        oci_close($this->conn);
        return $all;
    }

}
