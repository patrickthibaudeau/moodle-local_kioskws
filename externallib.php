<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * External Web Service Template
 *
 * @package    localwstemplate
 * @copyright  2011 Moodle Pty Ltd (http://moodle.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once($CFG->libdir . "/externallib.php");
require_once("$CFG->dirroot/config.php");
require_once("$CFG->dirroot/local/kioskws/classes/Oracle.php");

class local_kioskws_external extends external_api {
    //**************************** SEARCH USERS **********************

    /**
     * Return weather information.
     *
     * @return single_structure_description
     */
    public static function timetable_details() {
        $object = array(
            'seqcrsview' => new external_value(PARAM_INT, 'seqcrsview'),
            'academicyear' => new external_value(PARAM_INT, 'academicyear'),
            'periodfaculty' => new external_value(PARAM_TEXT, 'periodfaculty'),
            'period' => new external_value(PARAM_TEXT, 'period'),
            'faculty' => new external_value(PARAM_TEXT, 'faculty'),
            'subject' => new external_value(PARAM_TEXT, 'subject'),
            'courselevel' => new external_value(PARAM_INT, 'courselevel'),
            'coursenumber' => new external_value(PARAM_TEXT, 'coursenumber'),
            'sectionkey' => new external_value(PARAM_TEXT, 'sectionkey'),
            'instructionalformat1' => new external_value(PARAM_TEXT, 'instructionalformat1'),
            'dayofweek' => new external_value(PARAM_TEXT, 'dayofweek'),
            'hour' => new external_value(PARAM_TEXT, 'hour'),
            'minute' => new external_value(PARAM_TEXT, 'minute'),
            'duration' => new external_value(PARAM_INT, 'duration'),
            'building' => new external_value(PARAM_TEXT, 'building  '),
            'room' => new external_value(PARAM_TEXT, 'room'),
            'r25synchnum' => new external_value(PARAM_INT, 'r25synchnum', false),
            'r25synchstatus' => new external_value(PARAM_TEXT, 'r25synchstatus', false),
            'langofinstruction' => new external_value(PARAM_TEXT, 'langofinstruction'),
            'campus' => new external_value(PARAM_TEXT, 'campus'),
            'startdate' => new external_value(PARAM_INT, 'startdate'),
            'enddate' => new external_value(PARAM_INT, 'enddate'),
            'facultycyin' => new external_value(PARAM_INT, 'facultycyin', false),
            'firstname' => new external_value(PARAM_TEXT, 'firstname', false),
            'surname' => new external_value(PARAM_TEXT, 'surname', false),
            'salutation ' => new external_value(PARAM_TEXT, 'salutation ', false),
            'groupnumber' => new external_value(PARAM_TEXT, 'groupnumber', false),
            'creditweight' => new external_value(PARAM_INT, 'creditweight', false),
        );
        
        return new external_single_structure($object);
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function timetable_parameters() {
        return new external_function_parameters(
                array(
            'faculty' => new external_value(PARAM_TEXT, 'Faculty abbreviation (GL)'),
                )
        );
    }

    /**
     * Returns welcome message
     * @return string welcome message
     */
    public static function timetable($faculty) {
        global $USER, $DB;

        //Parameter validation
        //REQUIRED
        $params = self::validate_parameters(self::timetable_parameters(), array(
                    'faculty' => $faculty,
                        )
        );

        //Context validation
        //OPTIONAL but in most web service it should present
        $context = context_system::instance();
        self::validate_context($context);

        $ODB = new \local_kioskws\oracle();

        $day = [
            1 => 'M',
            2 => 'T',
            3 => 'W',
            4 => 'R',
            5 => 'F',
            6 => 'S',
            7 => 'U',
        ];

        /**
         * Get Period based on month
         */
        $period = '';
        switch (date('n', time())) {
            case 1:
            case 2:
            case 3:
            case 4:
                $period = "m.PERIOD IN ('Y', 'W')";
                $academicYear = "m.ACADEMICYEAR = " . (date('Y', time()) - 1);
                break;
            case 5:
            case 6:
            case 7:
            case 8:
                $period = "m.PERIOD NOT IN ('Y', 'F', 'W')";
                $academicYear = "m.ACADEMICYEAR = " . (date('Y', time()) - 1);
                break;
            case 9:
            case 10:
            case 11:
            case 12:
                $period = "m.PERIOD IN ('Y', 'F')";
                $academicYear = "m.ACADEMICYEAR = " . date('Y', time());
                break;
        }

        $dayOfWeek = "m.DAYOFWEEK = '" . $day[date('N', time())] . "'";
        $hour = "m.HOUR >= " . date('G', time());

        $sql = "Select ";
        $sql .= "    m.SEQCRSVIEW, ";
        $sql .= "    m.ACADEMICYEAR, ";
        $sql .= "    m.PERIODFACULTY, ";
        $sql .= "    m.PERIOD, ";
        $sql .= "    cv.FACULTY, ";
        $sql .= "    cv.SUBJECT, ";
        $sql .= "    cv.COURSELEVEL, ";
        $sql .= "    cv.COURSENUMBER, ";
        $sql .= "    mg.SECTIONKEY, ";
        $sql .= "    mg.INSTRUCTIONALFORMAT As INSTRUCTIONALFORMAT1, ";
        $sql .= "    m.DAYOFWEEK, ";
        $sql .= "    m.HOUR, ";
        $sql .= "    m.MINUTE, ";
        $sql .= "    m.DURATION, ";
        $sql .= "    m.BUILDING, ";
        $sql .= "    m.ROOM, ";
        $sql .= "    m.R25SYNCHNUM, ";
        $sql .= "    m.R25SYNCHSTATUS, ";
        $sql .= "    mg.LANGOFINSTRUCTION, ";
        $sql .= "    mg.CAMPUS, ";
        $sql .= "    vp.STARTDATE, ";
        $sql .= "    vp.ENDDATE, ";
        $sql .= "    i.FACULTYCYIN, ";
        $sql .= "    fm.FIRSTNAME, ";
        $sql .= "    fm.SURNAME, ";
        $sql .= "    fm.SALUTATION, ";
        $sql .= "    m.GROUPNUMBER, ";
        $sql .= "    cv.CREDITWEIGHT ";
        $sql .= "From ";
        $sql .= "    V222.MEETING m Inner Join ";
        $sql .= "    V222.COURSE_VIEW cv On m.SEQCRSVIEW = cv.SEQCRSVIEW Inner Join ";
        $sql .= "    V222.MEETING_GROUP mg On m.SEQCRSVIEW = mg.SEQCRSVIEW ";
        $sql .= "            And m.ACADEMICYEAR = mg.ACADEMICYEAR ";
        $sql .= "            And m.PERIODFACULTY = mg.PERIODFACULTY ";
        $sql .= "            And m.PERIOD = mg.PERIOD ";
        $sql .= "            And m.SECTIONKEY = mg.SECTIONKEY ";
        $sql .= "            And m.INSTRUCTIONALFORMAT = mg.INSTRUCTIONALFORMAT ";
        $sql .= "            And m.GROUPNUMBER = mg.GROUPNUMBER Inner Join ";
        $sql .= "    V222.VALID_PERIOD vp On m.PERIOD = vp.PERIOD ";
        $sql .= "            And m.PERIODFACULTY = vp.PERIODFACULTY ";
        $sql .= "            And m.ACADEMICYEAR = vp.ACADEMICYEAR Left Join ";
        $sql .= "    V222.INSTRUCTOR i On i.SEQCRSVIEW = mg.SEQCRSVIEW ";
        $sql .= "            And i.ACADEMICYEAR = mg.ACADEMICYEAR ";
        $sql .= "            And i.PERIODFACULTY = mg.PERIODFACULTY ";
        $sql .= "            And i.PERIOD = mg.PERIOD ";
        $sql .= "            And i.SECTIONKEY = mg.SECTIONKEY ";
        $sql .= "            And i.INSTRUCTIONALFORMAT = mg.INSTRUCTIONALFORMAT ";
        $sql .= "            And i.GROUPNUMBER = mg.GROUPNUMBER Left Join ";
        $sql .= "    V222.VIEW_FACULTY_MEMBER fm On i.FACULTYCYIN = fm.FACULTYCYIN ";
        $sql .= "Where ";
        $sql .= $academicYear . " And ";
        $sql .= "    m.PERIODFACULTY = '" . $faculty . "' And ";
        $sql .= $period . " And ";
        $sql .= $dayOfWeek;
//        $sql .= $dayOfWeek . " And ";
//        $sql .= $hour;
        $sql .= " Order By ";
        $sql .= "    cv.SUBJECT, ";
        $sql .= "    cv.COURSELEVEL, ";
        $sql .= "    cv.COURSENUMBER ";

        $timeTable = $ODB->get_records_sql($sql);

        $results = [];
        foreach ($timeTable as $key => $item) {
            if ($item['MINUTE'] == 0) {
                $minute = "00";
            } else {
                $minute = $item['MINUTE'];
            }
            $results[$key]['seqcrsview'] = $item['SEQCRSVIEW'];
            $results[$key]['academicyear'] = $item['ACADEMICYEAR'];
            $results[$key]['periodfaculty'] = $item['PERIODFACULTY'];
            $results[$key]['period'] = $item['PERIOD'];
            $results[$key]['faculty'] = $item['FACULTY'];
            $results[$key]['subject'] = $item['SUBJECT'];
            $results[$key]['courselevel'] = $item['COURSELEVEL'];
            $results[$key]['coursenumber'] = $item['COURSENUMBER'];
            $results[$key]['sectionkey'] = $item['SECTIONKEY'];
            $results[$key]['instructionalformat1'] = $item['INSTRUCTIONALFORMAT1'];
            $results[$key]['dayofweek'] = $item['DAYOFWEEK'];
            $results[$key]['hour'] = $item['HOUR'];
            $results[$key]['minute'] = $minute;
            $results[$key]['duration'] = $item['DURATION'];
            $results[$key]['building'] = trim($item['BUILDING']);
            $results[$key]['room'] = trim($item['ROOM']);
            $results[$key]['r25synchnum'] = $item['R25SYNCHNUM'];
            $results[$key]['r25synchstatus'] = $item['R25SYNCHSTATUS'];
            $results[$key]['langofinstruction'] = $item['LANGOFINSTRUCTION'];
            $results[$key]['campus'] = $item['CAMPUS'];
            $results[$key]['startdate'] = $item['STARTDATE'];
            $results[$key]['enddate'] = $item['ENDDATE'];
            $results[$key]['facultycyin'] = $item['FACULTYCYIN'];
            $results[$key]['firstname'] = $item['FIRSTNAME'];
            $results[$key]['surname'] = $item['SURNAME'];
            $results[$key]['salutation '] = $item['SALUTATION '];
            $results[$key]['groupnumber'] = $item['GROUPNUMBER'];
            $results[$key]['creditweight'] = $item['CREDITWEIGHT'];
        }
        return $results;
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function timetable_returns() {
        return new external_multiple_structure(self::timetable_details());
    }

}
