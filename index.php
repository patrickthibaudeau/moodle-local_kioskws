<?php
/**
 * *************************************************************************
 * *                              kioskws                               **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  kioskws                                                 **
 * @name        kioskws                                                 **
 * @copyright   oohoo.biz                                                 **
 * @link        http://oohoo.biz                                          **
 * @author                                                                **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * *************************************************************************/
require_once(dirname(__FILE__) . '../../../config.php');
require_once('locallib.php');

/**
 * Display the content of the page
 * @global stdobject $CFG
 * @global moodle_database $DB
 * @global core_renderer $OUTPUT
 * @global moodle_page $PAGE
 * @global stdobject $SESSION
 * @global stdobject $USER
 */
function display_page() {
    // CHECK And PREPARE DATA
    global $CFG, $OUTPUT, $SESSION, $PAGE, $DB, $COURSE, $USER;

    $id = optional_param('id', 0, PARAM_INT); //List id



    require_login(1, false); //Use course 1 because this has nothing to do with an actual course, just like course 1

    $context = context_system::instance();


    $pagetitle = get_string('pluginname', 'local_kioskws');
    $pageheading = get_string('pluginname', 'local_kioskws');

    echo local_kioskws_page('/index.php', $pagetitle, $pageheading, $context);

    $HTMLcontent = '';
    //**********************
    //*** DISPLAY HEADER ***
    //**********************
    echo $OUTPUT->header();
    //**********************
    //*** DISPLAY CONTENT **
    //**********************
    ?>
<div class="alert alert-danger"><H3>THIS PLUGIN ONLY HAS A WEBSERVICE</H3></div> 

    <?php
    //**********************
    //*** DISPLAY FOOTER ***
    //**********************
    echo $OUTPUT->footer();
}

display_page();
?>