<?php
/**
 * *************************************************************************
 * *                              kioskws                               **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  kioskws                                                 **
 * @name        kioskws                                                 **
 * @copyright   oohoo.biz                                                 **
 * @link        http://oohoo.biz                                          **
 * @author                                                                **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * *************************************************************************/
$plugins = array(
    'datatable' => array('files' => array(
            'datatable/DataTables-1.10.12/js/jquery.dataTables.min.js',
            'datatable/Select-1.2.0/js/dataTables.select.min.js',
            'datatable/Buttons-1.2.1/js/dataTables.buttons.min.js',
            'datatable/JSZip-2.5.0/jszip.min.js',
            'datatable/Buttons-1.2.1/js/buttons.html5.min.js',
            'datatable/DataTables-1.10.12/css/jquery.dataTables.min.css',
            'datatable/Select-1.2.0/css/select.dataTables.min.css',
            'datatable/Buttons-1.2.1/css/buttons.dataTables.min.css',
        )),
    'select2' => array('files' => array(
            'select2/dist/js/select2.min.js',
            'select2/dist/css/select2.min.css',
        )),
    'fontawesome' => array('files' => array(
            'font-awesome-4.6.3/dfont-awesome.min.css',
        )),
    'kioskws' => array('files' => array(
            'kioskws/js/kioskws.js',
            'kioskws/css/kioskws.css',
        )),
);
