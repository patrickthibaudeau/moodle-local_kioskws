<?php

/**
 * *************************************************************************
 * *                              kioskws                               **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  kioskws                                                 **
 * @name        kioskws                                                 **
 * @copyright   oohoo.biz                                                 **
 * @link        http://oohoo.biz                                          **
 * @author                                                                **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */
defined('MOODLE_INTERNAL') || die;

if ($hassiteconfig) {
    $settings = new admin_settingpage('local_kioskws', get_string('pluginname', 'local_kioskws'));
    $ADMIN->add('localplugins', $settings);
    $settings->add(new admin_setting_heading(
                    'oracle_heading', get_string('db_server_heading', 'local_kioskws'), ''
    ));
    $settings->add(new admin_setting_configtext(
                    'kioskwsDbHost', get_string('db_host', 'local_kioskws'), get_string('db_host_help', 'local_kioskws'), '', PARAM_TEXT
    ));
    $settings->add(new admin_setting_configtextarea(
        'kioskwsdbconnection', 'Oracle Connection', 'Oracle Connection ', ''));
    $settings->add(new admin_setting_configtext(
                    'kioskwsDbUsername', get_string('db_username', 'local_kioskws'), get_string('db_username_help', 'local_kioskws'), '', PARAM_TEXT
    ));
    $settings->add(new admin_setting_configpasswordunmask(
                    'kioskwsDbPwd', get_string('db_password', 'local_kioskws'), get_string('db_password_help', 'local_kioskws'), '', PARAM_TEXT
    ));
}