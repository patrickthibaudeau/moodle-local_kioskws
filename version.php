<?php
/**
 * *************************************************************************
 * *                              kioskws                               **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  kioskws                                                 **
 * @name        kioskws                                                 **
 * @copyright   oohoo.biz                                                 **
 * @link        http://oohoo.biz                                          **
 * @author                                                                **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * *************************************************************************/

$plugin->version = 2019072700;
// Moodle version required
$plugin->requires = 2010112400;
$plugin->maturity = MATURITY_ALPHA;
$plugin->release =  '0.0.1 (Build 2019072700)';
$plugin->cron = 0;
$plugin->component = 'local_kioskws';