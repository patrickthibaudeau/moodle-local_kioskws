<?php

/**
 * *************************************************************************
 * *                       Kiosk Web Services                             **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  kioskws                                                   **
 * @name        kioskws                                                   **
 * @copyright   Glendon ITS                                               **
 * @link        http://www.glendon.yorku.ca/its                           **
 * @author      Patrick Thibaudeau                                        **
 * @author      Glendon ITS                                               **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */

// We defined the web service functions to install.

$functions = array(
    'kioskws_timetable' => array(
        'classname' => 'local_kioskws_external',
        'methodname' => 'timetable',
        'classpath' => 'local/kioskws/externallib.php',
        'description' => 'Return course calendar based on academic year, faculty, period and day of the week',
        'type' => 'read',
        'capabilities' => ''
    ),
);
