<?php
$string["db_host"] = "Oracle host";
$string["db_host_help"] = "Oracle host ip or url";
$string["db_password"] = "Oracle password";
$string["db_password_help"] = "Oracle password for above user";
$string["db_server_heading"] = "Oracle configuration";
$string["db_username"] = "Oracle User name";
$string["db_username_help"] = "Oracle User name";
$string["pluginname"] = "Kiosk Webservice API";
