<?php
/**
 * *************************************************************************
 * *                              kioskws                               **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  kioskws                                                 **
 * @name        kioskws                                                 **
 * @copyright   oohoo.biz                                                 **
 * @link        http://oohoo.biz                                          **
 * @author                                                                **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */
require_once(dirname(__FILE__) . '../../../config.php');
require_once('locallib.php');
require_once("$CFG->dirroot/local/kioskws/classes/Oracle.php");

/**
 * Display the content of the page
 * @global stdobject $CFG
 * @global moodle_database $DB
 * @global core_renderer $OUTPUT
 * @global moodle_page $PAGE
 * @global stdobject $SESSION
 * @global stdobject $USER
 */
function display_page() {
    // CHECK And PREPARE DATA
    global $CFG, $OUTPUT, $SESSION, $PAGE, $DB, $COURSE, $USER;

    $id = optional_param('id', 0, PARAM_INT); //List id



    require_login(1, false); //Use course 1 because this has nothing to do with an actual course, just like course 1

    $context = context_system::instance();


    $pagetitle = get_string('pluginname', 'local_kioskws');
    $pageheading = get_string('pluginname', 'local_kioskws');

    echo local_kioskws_page('/test.php', $pagetitle, $pageheading, $context);

    $HTMLcontent = '';
    //**********************
    //*** DISPLAY HEADER ***
    //**********************
    echo $OUTPUT->header();
    //**********************
    //*** DISPLAY CONTENT **
    //**********************
    $ODB = new \local_kioskws\oracle('dsssisprod.sis.yorku.ca/SISDSS', 'thibaud', 'gl$t4ud');

    $day = [
        1 => 'M',
        2 => 'T',
        3 => 'W',
        4 => 'R',
        5 => 'F',
        6 => 'S',
        7 => 'U',
    ];

    $faculty = 'GL';
    /**
     * Get Period based on month
     */
    $period = '';
    switch (date('n', time())) {
        case 1:
        case 2:
        case 3:
        case 4:
            $period = "m.PERIOD IN ('Y', 'W')";
            $academicYear = "m.ACADEMICYEAR = " . (date('Y', time()) - 1);
            break;
        case 5:
        case 6:
        case 7:
        case 8:
            $period = "m.PERIOD NOT IN ('Y', 'F', 'W')";
            $academicYear = "m.ACADEMICYEAR = " . (date('Y', time()) - 1);
            break;
        case 9:
        case 10:
        case 11:
        case 12:
            $period = "m.PERIOD IN ('Y', 'F')";
            $academicYear = "m.ACADEMICYEAR = " . date('Y', time());
            break;
    }

    $dayOfWeek = "m.DAYOFWEEK = '" . $day[date('N', time())] . "'";
    $hour = "m.HOUR >= " . date('G', time());

    $sql = "Select " . "\n";
    $sql .= "    m.SEQCRSVIEW, " . "\n";
    $sql .= "    m.ACADEMICYEAR, " . "\n";
    $sql .= "    m.PERIODFACULTY, " . "\n";
    $sql .= "    m.PERIOD, " . "\n";
    $sql .= "    cv.FACULTY, " . "\n";
    $sql .= "    cv.SUBJECT, " . "\n";
    $sql .= "    cv.COURSELEVEL, " . "\n";
    $sql .= "    cv.COURSENUMBER, " . "\n";
    $sql .= "    mg.SECTIONKEY, " . "\n";
    $sql .= "    mg.INSTRUCTIONALFORMAT As INSTRUCTIONALFORMAT1, " . "\n";
    $sql .= "    m.DAYOFWEEK, " . "\n";
    $sql .= "    m.HOUR, " . "\n";
    $sql .= "    m.MINUTE, " . "\n";
    $sql .= "    m.DURATION, " . "\n";
    $sql .= "    m.BUILDING, " . "\n";
    $sql .= "    m.ROOM, " . "\n";
    $sql .= "    m.R25SYNCHNUM, " . "\n";
    $sql .= "    m.R25SYNCHSTATUS, " . "\n";
    $sql .= "    mg.LANGOFINSTRUCTION, " . "\n";
    $sql .= "    mg.CAMPUS, " . "\n";
    $sql .= "    vp.STARTDATE, " . "\n";
    $sql .= "    vp.ENDDATE, " . "\n";
    $sql .= "    i.FACULTYCYIN, " . "\n";
    $sql .= "    fm.FIRSTNAME, " . "\n";
    $sql .= "    fm.SURNAME, " . "\n";
    $sql .= "    fm.SALUTATION, " . "\n";
    $sql .= "    m.GROUPNUMBER, " . "\n";
    $sql .= "    cv.CREDITWEIGHT " . "\n";
    $sql .= "From " . "\n";
    $sql .= "    V222.MEETING m Inner Join " . "\n";
    $sql .= "    V222.COURSE_VIEW cv On m.SEQCRSVIEW = cv.SEQCRSVIEW Inner Join " . "\n";
    $sql .= "    V222.MEETING_GROUP mg On m.SEQCRSVIEW = mg.SEQCRSVIEW " . "\n";
    $sql .= "            And m.ACADEMICYEAR = mg.ACADEMICYEAR " . "\n";
    $sql .= "            And m.PERIODFACULTY = mg.PERIODFACULTY " . "\n";
    $sql .= "            And m.PERIOD = mg.PERIOD " . "\n";
    $sql .= "            And m.SECTIONKEY = mg.SECTIONKEY " . "\n";
    $sql .= "            And m.INSTRUCTIONALFORMAT = mg.INSTRUCTIONALFORMAT " . "\n";
    $sql .= "            And m.GROUPNUMBER = mg.GROUPNUMBER Inner Join " . "\n";
    $sql .= "    V222.VALID_PERIOD vp On m.PERIOD = vp.PERIOD " . "\n";
    $sql .= "            And m.PERIODFACULTY = vp.PERIODFACULTY " . "\n";
    $sql .= "            And m.ACADEMICYEAR = vp.ACADEMICYEAR Left Join " . "\n";
    $sql .= "    V222.INSTRUCTOR i On i.SEQCRSVIEW = mg.SEQCRSVIEW " . "\n";
    $sql .= "            And i.ACADEMICYEAR = mg.ACADEMICYEAR " . "\n";
    $sql .= "            And i.PERIODFACULTY = mg.PERIODFACULTY " . "\n";
    $sql .= "            And i.PERIOD = mg.PERIOD " . "\n";
    $sql .= "            And i.SECTIONKEY = mg.SECTIONKEY " . "\n";
    $sql .= "            And i.INSTRUCTIONALFORMAT = mg.INSTRUCTIONALFORMAT " . "\n";
    $sql .= "            And i.GROUPNUMBER = mg.GROUPNUMBER Left Join " . "\n";
    $sql .= "    V222.VIEW_FACULTY_MEMBER fm On i.FACULTYCYIN = fm.FACULTYCYIN " . "\n";
    $sql .= "Where " . "\n";
    $sql .= $academicYear . " And " . "\n";
    $sql .= "    m.PERIODFACULTY = '" . $faculty . "' And " . "\n";
    $sql .= $period . " And " . "\n";
//    $sql .= $dayOfWeek . " And " . "\n";
    $sql .= $hour;
    $sql .= " Order By " . "\n";
    $sql .= "    cv.SUBJECT, " . "\n";
    $sql .= "    cv.COURSELEVEL, " . "\n";
    $sql .= "    cv.COURSENUMBER " . "\n";


    $timeTable = $ODB->get_records_sql($sql);


    foreach ($timeTable as $key => $item) {
        $results[$key]['seqcrsview'] = $item['SEQCRSVIEW'];
        $results[$key]['academicyear'] = $item['ACADEMICYEAR'];
        $results[$key]['periodfaculty'] = $item['PERIODFACULTY'];
        $results[$key]['period'] = $item['PERIOD'];
        $results[$key]['faculty'] = $item['FACULTY'];
        $results[$key]['subject'] = $item['SUBJECT'];
        $results[$key]['courselevel'] = $item['COURSELEVEL'];
        $results[$key]['coursenumber'] = $item['COURSENUMBER'];
        $results[$key]['sectionkey'] = $item['SECTIONKEY'];
        $results[$key]['instructionalformat1'] = $item['INSTRUCTIONALFORMAT1'];
        $results[$key]['dayofweek'] = $item['DAYOFWEEK'];
        $results[$key]['hour'] = $item['HOUR'];
        $results[$key]['minute'] = $item['MINUTE'];
        $results[$key]['duration'] = $item['DURATION'];
        $results[$key]['building  '] = $item['BUILDING  '];
        $results[$key]['room    '] = $item['ROOM    '];
        $results[$key]['r25synchnum'] = $item['R25SYNCHNUM'];
        $results[$key]['r25synchstatus'] = $item['R25SYNCHSTATUS'];
        $results[$key]['langofinstruction'] = $item['LANGOFINSTRUCTION'];
        $results[$key]['campus'] = $item['CAMPUS'];
        $results[$key]['startdate'] = $item['STARTDATE'];
        $results[$key]['enddate'] = $item['ENDDATE'];
        $results[$key]['facultycyin'] = $item['FACULTYCYIN'];
        $results[$key]['firstname'] = $item['FIRSTNAME'];
        $results[$key]['surname'] = $item['SURNAME'];
        $results[$key]['salutation '] = $item['SALUTATION '];
        $results[$key]['groupnumber'] = $item['GROUPNUMBER'];
        $results[$key]['creditweight'] = $item['CREDITWEIGHT'];
    }
    print_object($results);
    ?>
    <div class="alert alert-danger"><H3>THIS PLUGIN ONLY HAS A WEBSERVICE</H3></div> 

    <?php
    //**********************
    //*** DISPLAY FOOTER ***
    //**********************
    echo $OUTPUT->footer();
}

display_page();
?>